const express = require("express");
const app = express();
const router = require("./routers");
const port = process.env.PORT || 3000;
const swaggerJSON = require("./swagger.json");
const swaggerUI = require("swagger-ui-express");
const bodyParser = require("body-parser");

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());
app.set("view engine", "ejs"); // set ejs
app.use(express.static("public"));
app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));

app.use(router);

app.get("/", (req, res) => {
  res.render("index");
});

// app.get("/user-games", (req, res) => {
//   res.render("usergame");
// });

// app.get("/add-user-games", (req, res) => {
//   res.render("add_usergame", { title: "Add User Game" });
// });

// app.get("/user-games-biodata", (req, res) => {
//   res.render("usergamebiodata");
// });

// app.get("/user-games-history", (req, res) => {
//   res.render("usergamehistory");
// });

app.listen(port, () => console.log(`Listening on port ${port}`));
