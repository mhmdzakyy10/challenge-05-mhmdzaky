const { UserGame } = require("../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
require("dotenv").config();

module.exports = {
  readAll: (req, res) => {
    UserGame.findAll({
      attributes: ["id", "username", "password", "createdAt", "updatedAt"],
    })
      .then((user_game) => {
        console.log(user_game);
        res.status(200).json({ message: "Success", data: user_game });
      })
      .catch((err) => {
        res.status(422).json({ message: "Error", err });
      });
  },
  readById: (req, res) => {
    UserGame.findOne({
      where: { id: req.params.id },
      attributes: ["id", "username", "password", "createdAt", "updatedAt"],
    })
      .then((user_game) => {
        res.status(200).json({ message: "Success", data: user_game });
      })
      .catch((err) => {
        res.status(422).json({ message: "Error", err });
      });
  },

  create: async (req, res) => {
    try {
      const { username, password } = req.body;
      const hashedPassword = await bcrypt.hash(password, 10);
      const user = await UserGame.create({
        username,
        password: hashedPassword,
      });
      const token = jwt.sign({ id: user.id }, process.env.TOKEN_KEY, {
        expiresIn: "15m",
      });
      user.token = token;

      res.status(200).json({ message: "Berhasil Membuat User Game", result: user });
    } catch (error) {
      res.status(500).json({ message: "Gagal Create User Game", err: error.message });
    }
  },
  update: (req, res) => {
    const { username, password } = req.body;
    UserGame.update({ username, password }, { where: { id: req.params.id } })
      .then((user_game) => {
        res.status(200).json({ message: "User Game Updated", data: user_game });
      })
      .catch((err) => {
        res.status(422).json({ message: "Error updating user game", err });
      });
  },
  delete: (req, res) => {
    UserGame.destroy({ where: { id: req.params.id } })
      .then((user_game) => {
        res.status(200).json({ message: "User Game Deleted", data: user_game });
      })
      .catch((err) => {
        res.status(422).json({ message: "Error deleting user game", err });
      });
  },
};
